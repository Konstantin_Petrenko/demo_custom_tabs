package com.android.testpager.views.fragments

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.testpager.databinding.FragmentWebviewBinding

private const val PATTERNS = "https://refactoring.guru/ru/design-patterns"

class SecondFragment : BaseFragment<FragmentWebviewBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) ->
    FragmentWebviewBinding = FragmentWebviewBinding::inflate

    @SuppressLint("SetJavaScriptEnabled")
    override fun setup() {
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.loadUrl(PATTERNS)
    }
}