package com.android.testpager.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.android.testpager.views.fragments.FirstFragment
import com.android.testpager.views.fragments.SecondFragment
import com.android.testpager.views.fragments.ThirdFragment

private const val VIEW_PAGER_SIZE = 3
private const val FIRST = 0
private const val SECOND = 1

class ViewPagerAdapter(fm: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fm, lifecycle) {

    override fun getItemCount() = VIEW_PAGER_SIZE

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            FIRST -> FirstFragment()
            SECOND -> SecondFragment()
            else -> ThirdFragment()
        }
    }
}